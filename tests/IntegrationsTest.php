<?php
require_once 'vendor/autoload.php';

class PagesIntegrationTest extends IntegrationTest{

    public function test_index()
    {
        $response = $this->make_request("GET", "/");
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertContain("Home", $response->getBody()->getContents());
        $this->assertContains("text/html", $response->getHeader('Content-Type')[0]);
    }

    public function test_dino()
    {
        $response = $this->make_request("GET", "/dinosaurs/brachiosaurus");
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertContains("avatar: https://allosaurus.delahayeyourself.info/static/img/dinosaurs/brachiosaurus.jpg", $response->getBody()->getContents());
        $this->assertContains("Brachiosaurus was considered", $response->getBody()->getContents());
        $this->assertContains("weight:30000", $response->getBody()->getContents());
        $this->assertContains("length:27", $response->getBody()->getContents());
        $this->assertContains("height:9", $response->getBody()->getContents());
        $this->assertContains("diet:Herbivore", $response->getBody()->getContents());
        $this->assertContains("text/html", $response->getHeader('Content-Type')[0]);
    }
}